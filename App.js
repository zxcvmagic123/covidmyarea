import React, { Component } from 'react';
import {View} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


import HomeScreen from './src/screens/Home'
import ProvinceDetails from './src/screens/ProvinceDetail'


const Stack = createStackNavigator();

export default class App extends Component {
   

  render(){
    return(
      <View style={{flex:1,}}>
      <NavigationContainer>
        <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="ProvinceDetails" component={ProvinceDetails} />
        </Stack.Navigator>
        </NavigationContainer>
        </View>
    )
  }
}
